From 76445f36a2f9e41b1744d0327e7ec243cb7fac12 Mon Sep 17 00:00:00 2001
From: Jens Remus <jremus@linux.ibm.com>
Date: Mon, 18 Nov 2024 10:42:21 +0100
Subject: [PATCH] s390: Add arch15 Concurrent-Functions Facility insns

Commit: 76445f36a2f9

opcodes/
	* s390-opc.txt: Add arch15 Concurrent-Functions Facility
	instructions.
	* s390-opc.c (INSTR_SSF_RRDRD2, MASK_SSF_RRDRD2): New SSF
	instruction format variant.

gas/testsuite/
	* gas/s390/zarch-arch15.d: Tests for arch15 Concurrent-Functions
	Facility instructions.
	* gas/s390/zarch-arch15.s: Likewise.

Signed-off-by: Jens Remus <jremus@linux.ibm.com>
---
 gas/testsuite/gas/s390/zarch-arch15.d | 4 ++++
 gas/testsuite/gas/s390/zarch-arch15.s | 4 ++++
 opcodes/s390-opc.c                    | 2 ++
 opcodes/s390-opc.txt                  | 8 ++++++++
 4 files changed, 18 insertions(+)

diff --git a/gas/testsuite/gas/s390/zarch-arch15.d b/gas/testsuite/gas/s390/zarch-arch15.d
index 955c9706b35..9cd99b7a698 100644
--- a/gas/testsuite/gas/s390/zarch-arch15.d
+++ b/gas/testsuite/gas/s390/zarch-arch15.d
@@ -100,3 +100,7 @@ Disassembly of section .text:
 .*:	e6 0f 00 00 00 5f [	 ]*vtp	%v15
 .*:	e6 0f 0f ff d0 5f [	 ]*vtp	%v15,65533
 .*:	e6 0f 1f ff d2 7f [	 ]*vtz	%v15,%v17,65533
+.*:	c8 36 10 0a 20 14 [	 ]*cal	%r3,10\(%r1\),20\(%r2\)
+.*:	c8 37 10 0a 20 14 [	 ]*calg	%r3,10\(%r1\),20\(%r2\)
+.*:	c8 3f 10 0a 20 14 [	 ]*calgf	%r3,10\(%r1\),20\(%r2\)
+.*:	eb 13 28 f0 fd 16 [	 ]*pfcr	%r1,%r3,-10000\(%r2\)
diff --git a/gas/testsuite/gas/s390/zarch-arch15.s b/gas/testsuite/gas/s390/zarch-arch15.s
index 43be9d46a48..d9b89652fcb 100644
--- a/gas/testsuite/gas/s390/zarch-arch15.s
+++ b/gas/testsuite/gas/s390/zarch-arch15.s
@@ -94,3 +94,7 @@ foo:
 	vtp	%v15
 	vtp	%v15,65533
 	vtz	%v15,%v17,65533
+	cal	%r3,10(%r1),20(%r2)
+	calg	%r3,10(%r1),20(%r2)
+	calgf	%r3,10(%r1),20(%r2)
+	pfcr	%r1,%r3,-10000(%r2)
diff --git a/opcodes/s390-opc.c b/opcodes/s390-opc.c
index 49efd714157..23c1c3a24e5 100644
--- a/opcodes/s390-opc.c
+++ b/opcodes/s390-opc.c
@@ -468,6 +468,7 @@ unused_s390_operands_static_asserts (void)
 #define INSTR_SS_RRRDRD2   6, { R_8,D_20,B_16,R_12,D_36,B_32 }   /* e.g. plo   */
 #define INSTR_SS_RRRDRD3   6, { R_8,R_12,D_20,B_16,D_36,B_32 }   /* e.g. lmd   */
 #define INSTR_SSF_RRDRD    6, { D_20,B_16,D_36,B_32,R_8,0 }      /* e.g. mvcos */
+#define INSTR_SSF_RRDRD2   6, { R_8,D_20,B_16,D_36,B_32,0 }      /* e.g. cal   */
 #define INSTR_SSF_RERDRD2  6, { RE_8,D_20,B_16,D_36,B_32,0 }     /* e.g. lpd   */
 #define INSTR_S_00         4, { 0,0,0,0,0,0 }                    /* e.g. hsch  */
 #define INSTR_S_RD         4, { D_20,B_16,0,0,0,0 }              /* e.g. stck  */
@@ -700,6 +701,7 @@ unused_s390_operands_static_asserts (void)
 #define MASK_SS_RRRDRD2   { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00 }
 #define MASK_SS_RRRDRD3   { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00 }
 #define MASK_SSF_RRDRD    { 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00 }
+#define MASK_SSF_RRDRD2   { 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00 }
 #define MASK_SSF_RERDRD2  { 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00 }
 #define MASK_S_00         { 0xff, 0xff, 0xff, 0xff, 0x00, 0x00 }
 #define MASK_S_RD         { 0xff, 0xff, 0x00, 0x00, 0x00, 0x00 }
diff --git a/opcodes/s390-opc.txt b/opcodes/s390-opc.txt
index 82d6f06a992..68d8896bf4e 100644
--- a/opcodes/s390-opc.txt
+++ b/opcodes/s390-opc.txt
@@ -2190,3 +2190,11 @@ e6000000004a vcvdq VRI_VV0UU "vector convert to decimal 128 bit" arch15 zarch
 
 e6000000005f vtp VRR_0V0U "vector test decimal" arch15 zarch optparm
 e6000000007f vtz VRR_0VVU "vector test zoned" arch15 zarch
+
+# Concurrent-Functions Facility
+
+c806 cal SSF_RRDRD2 "compare and load 32" arch15 zarch
+c807 calg SSF_RRDRD2 "compare and load 64" arch15 zarch
+c80f calgf SSF_RRDRD2 "compare and load 64<32" arch15 zarch
+
+eb0000000016 pfcr RSY_RRRD "perform functions with concurrent results" arch15 zarch
-- 
2.47.0

