From fca086d928a940dc5aa3b5c0586bc5ed37d6b374 Mon Sep 17 00:00:00 2001
From: Jens Remus <jremus@linux.ibm.com>
Date: Thu, 23 Nov 2023 15:46:46 +0100
Subject: [PATCH] s390: Align optional operand definition to specs

Commit: fca086d928a9

The IBM z/Architecture Principle of Operation [1] specifies the last
operand(s) of some (extended) mnemonics to be optional. Align the
mnemonic definitions in the opcode table according to specification.

This changes the last operand of the following (extended) mnemonics to
be optional:
risbg, risbgz, risbgn, risbgnz, risbhg, risblg, rnsbg, rosbg, rxsbg

Note that efpc and sfpc actually have only one operand, but had
erroneously been defined to have two. For backwards compatibility the
wrong RR register format must be retained. Since the superfluous second
operand is defined as optional the instruction can still be coded as
specified.

[1]: IBM z/Architecture Principles of Operation, SA22-7832-13, IBM z16,
     https://publibfp.dhe.ibm.com/epubs/pdf/a227832d.pdf

opcodes/
	* s390-opc.txt: Align optional operand definition to
	specification.

testsuite/
	* zarch-z10.s: Add test cases for risbg, risbgz, rnsbg, rosbg,
	  and rxsbg.
	* zarch-z10.d: Likewise.
	* zarch-z196.s: Add test cases for risbhg and risblg.
	* zarch-z196.d: Likewise.
	* zarch-zEC12.s: Add test cases for risbgn and risbgnz.
	* zarch-zEC12.d: Likewise.

Signed-off-by: Jens Remus <jremus@linux.ibm.com>
Reviewed-by: Andreas Krebbel <krebbel@linux.ibm.com>
---
 gas/testsuite/gas/s390/zarch-z10.d   |  6 ++++++
 gas/testsuite/gas/s390/zarch-z10.s   |  6 ++++++
 gas/testsuite/gas/s390/zarch-z196.d  |  2 ++
 gas/testsuite/gas/s390/zarch-z196.s  |  2 ++
 gas/testsuite/gas/s390/zarch-zEC12.d | 18 ++++++++----------
 gas/testsuite/gas/s390/zarch-zEC12.s |  3 +++
 opcodes/s390-opc.txt                 | 20 +++++++++++---------
 7 files changed, 38 insertions(+), 19 deletions(-)

diff --git a/gas/testsuite/gas/s390/zarch-z10.d b/gas/testsuite/gas/s390/zarch-z10.d
index 2c7c485a1a1..4aca8372363 100644
--- a/gas/testsuite/gas/s390/zarch-z10.d
+++ b/gas/testsuite/gas/s390/zarch-z10.d
@@ -360,11 +360,17 @@ Disassembly of section .text:
 .*:	e3 a6 75 b3 01 36 [	 ]*pfd	10,5555\(%r6,%r7\)
  *([\da-f]+):	c6 a2 00 00 00 00 [	 ]*pfdrl	10,\1 <foo\+0x\1>
 .*:	ec 67 d2 dc e6 54 [	 ]*rnsbg	%r6,%r7,210,220,230
+.*:	ec 67 d2 dc 00 54 [	 ]*rnsbg	%r6,%r7,210,220
 .*:	ec 67 d2 dc e6 57 [	 ]*rxsbg	%r6,%r7,210,220,230
+.*:	ec 67 d2 dc 00 57 [	 ]*rxsbg	%r6,%r7,210,220
 .*:	ec 67 d2 dc e6 56 [	 ]*rosbg	%r6,%r7,210,220,230
+.*:	ec 67 d2 dc 00 56 [	 ]*rosbg	%r6,%r7,210,220
 .*:	ec 67 d2 14 e6 55 [	 ]*risbg	%r6,%r7,210,20,230
+.*:	ec 67 d2 14 00 55 [	 ]*risbg	%r6,%r7,210,20
 .*:	ec 67 d2 bc e6 55 [	 ]*risbgz	%r6,%r7,210,60,230
+.*:	ec 67 d2 bc 00 55 [	 ]*risbgz	%r6,%r7,210,60
 .*:	ec 67 d2 94 e6 55 [	 ]*risbgz	%r6,%r7,210,20,230
+.*:	ec 67 d2 94 00 55 [	 ]*risbgz	%r6,%r7,210,20
  *([\da-f]+):	c4 6f 00 00 00 00 [	 ]*strl	%r6,\1 <foo\+0x\1>
  *([\da-f]+):	c4 6b 00 00 00 00 [	 ]*stgrl	%r6,\1 <foo\+0x\1>
  *([\da-f]+):	c4 67 00 00 00 00 [	 ]*sthrl	%r6,\1 <foo\+0x\1>
diff --git a/gas/testsuite/gas/s390/zarch-z10.s b/gas/testsuite/gas/s390/zarch-z10.s
index 5cfc533d4d1..3ed61a4f0fd 100644
--- a/gas/testsuite/gas/s390/zarch-z10.s
+++ b/gas/testsuite/gas/s390/zarch-z10.s
@@ -354,11 +354,17 @@ foo:
 	pfd	10,5555(%r6,%r7)
 	pfdrl	10,.
 	rnsbg	%r6,%r7,210,220,230
+	rnsbg	%r6,%r7,210,220
 	rxsbg	%r6,%r7,210,220,230
+	rxsbg	%r6,%r7,210,220
 	rosbg	%r6,%r7,210,220,230
+	rosbg	%r6,%r7,210,220
 	risbg	%r6,%r7,210,20,230
+	risbg	%r6,%r7,210,20
 	risbg	%r6,%r7,210,188,230
+	risbg	%r6,%r7,210,188
 	risbgz	%r6,%r7,210,20,230
+	risbgz	%r6,%r7,210,20
 	strl	%r6,.
 	stgrl	%r6,.
 	sthrl	%r6,.
diff --git a/gas/testsuite/gas/s390/zarch-z196.d b/gas/testsuite/gas/s390/zarch-z196.d
index 0f8bfeafa25..b9db65f43ee 100644
--- a/gas/testsuite/gas/s390/zarch-z196.d
+++ b/gas/testsuite/gas/s390/zarch-z196.d
@@ -29,7 +29,9 @@ Disassembly of section .text:
 .*:	e3 67 8a 4d fe c2 [	 ]*llch	%r6,-5555\(%r7,%r8\)
 .*:	e3 67 8a 4d fe c6 [	 ]*llhh	%r6,-5555\(%r7,%r8\)
 .*:	ec 67 0c 0d 0e 5d [	 ]*risbhg	%r6,%r7,12,13,14
+.*:	ec 67 0c 0d 00 5d [	 ]*risbhg	%r6,%r7,12,13
 .*:	ec 67 0c 0d 0e 51 [	 ]*risblg	%r6,%r7,12,13,14
+.*:	ec 67 0c 0d 00 51 [	 ]*risblg	%r6,%r7,12,13
 .*:	e3 67 8a 4d fe c3 [	 ]*stch	%r6,-5555\(%r7,%r8\)
 .*:	e3 67 8a 4d fe c7 [	 ]*sthh	%r6,-5555\(%r7,%r8\)
 .*:	e3 67 8a 4d fe cb [	 ]*stfh	%r6,-5555\(%r7,%r8\)
diff --git a/gas/testsuite/gas/s390/zarch-z196.s b/gas/testsuite/gas/s390/zarch-z196.s
index 31be5a62dba..213c2a1e676 100644
--- a/gas/testsuite/gas/s390/zarch-z196.s
+++ b/gas/testsuite/gas/s390/zarch-z196.s
@@ -23,7 +23,9 @@ foo:
 	llch	%r6,-5555(%r7,%r8)
 	llhh	%r6,-5555(%r7,%r8)
 	risbhg	%r6,%r7,12,13,14
+	risbhg	%r6,%r7,12,13
 	risblg	%r6,%r7,12,13,14
+	risblg	%r6,%r7,12,13
 	stch	%r6,-5555(%r7,%r8)
 	sthh	%r6,-5555(%r7,%r8)
 	stfh	%r6,-5555(%r7,%r8)
diff --git a/gas/testsuite/gas/s390/zarch-zEC12.d b/gas/testsuite/gas/s390/zarch-zEC12.d
index 96bf59b9fb0..57d7becdcdf 100644
--- a/gas/testsuite/gas/s390/zarch-zEC12.d
+++ b/gas/testsuite/gas/s390/zarch-zEC12.d
@@ -47,8 +47,11 @@ Disassembly of section .text:
 .*:	eb 6c 7a 4d fe 2b [	 ]*clgtnh	%r6,-5555\(%r7\)
 .*:	eb 6c 7a 4d fe 2b [	 ]*clgtnh	%r6,-5555\(%r7\)
 .*:	ec 67 0c 0d 0e 59 [	 ]*risbgn	%r6,%r7,12,13,14
+.*:	ec 67 0c 0d 00 59 [	 ]*risbgn	%r6,%r7,12,13
 .*:	ec 67 0c bc 0e 59 [	 ]*risbgnz	%r6,%r7,12,60,14
+.*:	ec 67 0c bc 00 59 [	 ]*risbgnz	%r6,%r7,12,60
 .*:	ec 67 0c 94 0e 59 [	 ]*risbgnz	%r6,%r7,12,20,14
+.*:	ec 67 0c 94 00 59 [	 ]*risbgnz	%r6,%r7,12,20
 .*:	ed 0f 8f a0 6d aa [	 ]*cdzt	%f6,4000\(16,%r8\),13
 .*:	ed 21 8f a0 4d ab [	 ]*cxzt	%f4,4000\(34,%r8\),13
 .*:	ed 0f 8f a0 6d a8 [	 ]*czdt	%f6,4000\(16,%r8\),13
@@ -56,16 +59,11 @@ Disassembly of section .text:
 .*:	b2 e8 c0 56 [	 ]*ppa	%r5,%r6,12
 .*:	b9 8f 60 59 [	 ]*crdte	%r5,%r6,%r9
 .*:	b9 8f 61 59 [	 ]*crdte	%r5,%r6,%r9,1
-.*:	c5 a0 0c 00 00 0c [	 ]*bprp	10,136 <bar>,136 <bar>
+.*:	c5 a0 0c 00 00 0c [	 ]*bprp	10,148 <bar>,148 <bar>
  *([\da-f]+):	c5 a0 00 00 00 00 [	 ]*bprp	10,\1 <foo\+0x\1>,\1 <foo\+0x\1>
-[	 ]*125: R_390_PLT12DBL	bar\+0x1
-[	 ]*127: R_390_PLT24DBL	bar\+0x3
+[	 ]*137: R_390_PLT12DBL	bar\+0x1
+[	 ]*139: R_390_PLT24DBL	bar\+0x3
  *([\da-f]+):	c7 a0 00 00 00 00 [	 ]*bpp	10,\1 <foo\+0x\1>,0
-[	 ]*12e: R_390_PLT16DBL	bar\+0x4
+[	 ]*140: R_390_PLT16DBL	bar\+0x4
  *([\da-f]+):	c7 a0 00 00 00 00 [	 ]*bpp	10,\1 <foo\+0x\1>,0
-[	 ]*134: R_390_PC16DBL	baz\+0x4
-
-
-0000000000000136 <bar>:
-
-.*:	07 07 [	 ]*nopr	%r7
+[	 ]*146: R_390_PC16DBL	baz\+0x4
diff --git a/gas/testsuite/gas/s390/zarch-zEC12.s b/gas/testsuite/gas/s390/zarch-zEC12.s
index 03b577e4830..3a30e8d72bf 100644
--- a/gas/testsuite/gas/s390/zarch-zEC12.s
+++ b/gas/testsuite/gas/s390/zarch-zEC12.s
@@ -44,8 +44,11 @@ foo:
 	clgtnh	%r6,-5555(%r7)
 
 	risbgn	%r6,%r7,12,13,14
+	risbgn	%r6,%r7,12,13
 	risbgn	%r6,%r7,12,188,14
+	risbgn	%r6,%r7,12,188
 	risbgnz	%r6,%r7,12,20,14
+	risbgnz	%r6,%r7,12,20
 
 	cdzt	%f6,4000(16,%r8),13
 	cxzt	%f4,4000(34,%r8),13
diff --git a/opcodes/s390-opc.txt b/opcodes/s390-opc.txt
index b7efa813e47..9aeb2cc7ad1 100644
--- a/opcodes/s390-opc.txt
+++ b/opcodes/s390-opc.txt
@@ -305,6 +305,7 @@ b30d debr RRE_FF "divide short bfp" g5 esa,zarch
 ed000000000d deb RXE_FRRD "divide short bfp" g5 esa,zarch
 b35b didbr RRF_FUFF "divide to integer long bfp" g5 esa,zarch
 b353 diebr RRF_FUFF "divide to integer short bfp" g5 esa,zarch
+# efpc and sfpc have only one operand; retain RR register format for compatibility
 b38c efpc RRE_RR "extract fpc" g5 esa,zarch optparm
 b342 ltxbr RRE_FEFE "load and test extended bfp" g5 esa,zarch
 b312 ltdbr RRE_FF "load and test long bfp" g5 esa,zarch
@@ -348,6 +349,7 @@ b31f msdbr RRF_F0FF "multiply and subtract long bfp" g5 esa,zarch
 ed000000001f msdb RXF_FRRDF "multiply and subtract long bfp" g5 esa,zarch
 b30f msebr RRF_F0FF "multiply and subtract short bfp" g5 esa,zarch
 ed000000000f mseb RXF_FRRDF "multiply and subtract short bfp" g5 esa,zarch
+# efpc and sfpc have only one operand; retain RR register format for compatibility
 b384 sfpc RRE_RR "set fpc" g5 esa,zarch optparm
 b299 srnm S_RD "set rounding mode" g5 esa,zarch
 b316 sqxbr RRE_FEFE "square root extended bfp" g5 esa,zarch
@@ -966,11 +968,11 @@ c201 msfi RIL_RI "multiply single immediate (32)" z10 zarch
 c200 msgfi RIL_RI "multiply single immediate (64)" z10 zarch
 e30000000036 pfd RXY_URRD "prefetch data" z10 zarch
 c602 pfdrl RIL_UP "prefetch data relative long" z10 zarch
-ec0000000054 rnsbg RIE_RRUUU "rotate then and selected bits" z10 zarch
-ec0000000057 rxsbg RIE_RRUUU "rotate then exclusive or selected bits" z10 zarch
-ec0000000056 rosbg RIE_RRUUU "rotate then or selected bits" z10 zarch
-ec0000000055 risbg RIE_RRUUU "rotate then insert selected bits" z10 zarch
-ec0000800055 risbgz RIE_RRUUU2 "rotate then insert selected bits and zero remaining bits" z10 zarch
+ec0000000054 rnsbg RIE_RRUUU "rotate then and selected bits" z10 zarch optparm
+ec0000000057 rxsbg RIE_RRUUU "rotate then exclusive or selected bits" z10 zarch optparm
+ec0000000056 rosbg RIE_RRUUU "rotate then or selected bits" z10 zarch optparm
+ec0000000055 risbg RIE_RRUUU "rotate then insert selected bits" z10 zarch optparm
+ec0000800055 risbgz RIE_RRUUU2 "rotate then insert selected bits and zero remaining bits" z10 zarch optparm
 c40f strl RIL_RP "store relative long (32)" z10 zarch
 c40b stgrl RIL_RP "store relative long (64)" z10 zarch
 c407 sthrl RIL_RP "store halfword relative long" z10 zarch
@@ -1014,8 +1016,8 @@ e300000000c4 lhh RXY_RRRD "load halfword high" z196 zarch
 e300000000ca lfh RXY_RRRD "load high" z196 zarch
 e300000000c2 llch RXY_RRRD "load logical character high" z196 zarch
 e300000000c6 llhh RXY_RRRD "load logical halfword high" z196 zarch
-ec000000005d risbhg RIE_RRUUU "rotate then insert selected bits high" z196 zarch
-ec0000000051 risblg RIE_RRUUU "rotate then insert selected bits low" z196 zarch
+ec000000005d risbhg RIE_RRUUU "rotate then insert selected bits high" z196 zarch optparm
+ec0000000051 risblg RIE_RRUUU "rotate then insert selected bits low" z196 zarch optparm
 e300000000c3 stch RXY_RRRD "store character high" z196 zarch
 e300000000c7 sthh RXY_RRRD "store halfword high" z196 zarch
 e300000000cb stfh RXY_RRRD "store high" z196 zarch
@@ -1153,8 +1155,8 @@ eb0000000023 clt RSY_RURD "compare logical and trap 32 bit reg-mem" zEC12 zarch
 eb0000000023 clt$12 RSY_R0RD "compare logical and trap 32 bit reg-mem" zEC12 zarch
 eb000000002b clgt RSY_RURD "compare logical and trap 64 bit reg-mem" zEC12 zarch
 eb000000002b clgt$12 RSY_R0RD "compare logical and trap 64 bit reg-mem" zEC12 zarch
-ec0000000059 risbgn RIE_RRUUU "rotate then insert selected bits nocc" zEC12 zarch
-ec0000800059 risbgnz RIE_RRUUU2 "rotate then insert selected bits and zero remaining bits nocc" zEC12 zarch
+ec0000000059 risbgn RIE_RRUUU "rotate then insert selected bits nocc" zEC12 zarch optparm
+ec0000800059 risbgnz RIE_RRUUU2 "rotate then insert selected bits and zero remaining bits nocc" zEC12 zarch optparm
 ed00000000aa cdzt RSL_LRDFU "convert from zoned long" zEC12 zarch
 ed00000000ab cxzt RSL_LRDFEU "convert from zoned extended" zEC12 zarch
 ed00000000a8 czdt RSL_LRDFU "convert to zoned long" zEC12 zarch
-- 
2.47.0

