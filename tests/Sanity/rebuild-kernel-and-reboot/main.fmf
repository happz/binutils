summary: Check whether newly built kernel boots correctly
description: |
    Rebuild kernel, install it, reboot, and check if we're running the correct
    kernel. Tailored specificaly for binutils buildroot testing process.

    A strong machine is recommended:

      - kernel rebuild is very resoruce-intensive task, and having more powerful
        boxes for it is simply good,
      - this task will get its own boxes, not clobbered by additional kernel packages
        that are usually installed by other tasks in the same run. E.g.kernel-debuginfo,
        when installed, will conflict with freshly build kernel packages. This should
        workaround such situations,
      - this tasks reboots its boxes - should such reboot break something, don't ruin
        the whole run by it, right?

    Based on gcc/Sanity/rebuild-kernel by:
    Author: Michal Nowak <mnowak@redhat.com>
    Author: Marek Polacek <polacek@redhat.com>
contact:
- Milos Prchlik <mprchlik@redhat.com>
component:
- binutils
tag:
- gate-build
test: ./runtest.sh
framework: beakerlib
require:
- binutils
- gcc
- yum-utils
- rng-tools
- rpm-build
- newt-devel
- python-devel
- perl-ExtUtils-Embed
- unifdef
- elfutils-libelf-devel
- elfutils-devel
- pciutils-devel
- wget
- hmaccalc
- binutils-devel
- glibc-static
- texinfo
- gdb
- ecj
- graphviz
- gmp-devel
- mpfr-devel
- xmlto
- asciidoc
- net-tools
duration: 20h

adjust:
  - because: "pesign is available for some architectures only"
    when: distro == rhel and arch != ppc64le and arch != s390x
    require+:
      - pesign

# Dropping TCMS integration, these links should not be needed anymore.
# extra-summary: /tools/binutils/Sanity/rebuild-kernel-and-reboot
# extra-task: /tools/binutils/Sanity/rebuild-kernel-and-reboot
