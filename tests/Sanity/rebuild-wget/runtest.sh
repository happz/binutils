#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/binutils/Sanity/rebuild-wget
#   Description: Rebuild wget
#   Author: Milos Prchlik <mprchlik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1
 
LD="${LD:-/usr/bin/ld}"
GCC="${GCC:-/usr/bin/gcc}"
 
BINUTILS_PACKAGE="$(rpm --qf '%{name}\n' -qf "$(which "$LD")" | head -1)"
GCC_PACKAGE="$(rpm --qf '%{name}\n' -qf "$(which "$GCC")" | head -1)"

PACKAGES="${PACKAGES:-$BINUTILS_PACKAGE}"
REQUIRES="${REQUIRES:-$GCC_PACKAGE wget}"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "REQUIRES=$REQUIRES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "LD=$LD"
        rlLogInfo "GCC=$GCC"

        rlAssertRpm --all

        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled "wget"
        rlRun "SRPM=`find . -name 'wget-*.src.rpm'`"
        rlRun "rpm -ivh --define='_topdir $TmpDir' $SRPM"
        rlRun "SPECFILE=`find $TmpDir/ -name '*.spec'`"
        rlRun "dnf builddep -y $SPECFILE"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "rpmbuild --define='_topdir $TmpDir' -bb --clean $SPECFILE"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
